const DB = require('./DB/connector');
const HistoryModel = require('./DB/history');
let express = require('express');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let middleware = require('./middleware/middleware-init');
let server = express();
let bootstrap_modules = require('./modules/bootstrap_modules');


server.use(morgan('dev'));
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

middleware(server);
bootstrap_modules(server);

server.listen(3000, () => {
	console.log('Server started!');
});