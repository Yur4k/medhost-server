const Schema = require('./connector').Schema;
const Model = require('./connector').model;


let Doctor = Model('Doctor', new Schema({
    fio: {
        type: String,
        required: true,
    },
    sex: {
        type: Boolean,
        required: true,
    },
    position: {
        type: String,
        required: true
    },
    birthdate: {
        type: Date,
        required: true
    },
    contact_info: {
        login: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        phones: [{
            type: String,
            required: true,
        }],
        email: String
    },
    location_info: {
        state: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        street: {
            type: String,
            required: true
        },
        home: {
            type: String,
            required: true
        },
    }
}));

module.exports = Doctor;