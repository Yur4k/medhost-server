const Schema = require('./connector').Schema;
const Model = require('./connector').model;


let History = Model('History', new Schema({
    user_id: {
        type: Schema.ObjectId,
        required: true
    },
    history_book: [{
        doctor_id: Schema.ObjectId,
        started_at: Date,
        ended_at: Date,
        title: String,
        results: [{
            type: {
                type: String,
                required: true
            },
            overview: {
                type: String,
                required: true
            }
        }],
        recipies: [{
            desc: {
                type: String,
                required: true
            },
            recipie: {
                type: String,
                required: true
            }
        }]
    }]

}));

module.exports = History;