const Schema = require('./connector').Schema;
const Model = require('./connector').model;


let User = Model('User', new Schema({
    fio: {
        type: String,
        required: true,
    },
    sex: {
        type: Boolean, // man - 1,
        required: true,
    },
    workplace: {
        type: String,
        required: true
    },
    position: {
        type: String,
        required: true
    },
    birthdate: {
        type: Date,
        required: true
    },
    allowed: Schema.ObjectId,
    access_code: Number,
    weight: Number,
    growth: Number,
    blood: Number,
    contact_info: {
        phones: [{
            type: String,
            required: true,
        }],
        email: String,
    },
    location_info: {
        state: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        street: {
            type: String,
            required: true
        },
        home: {
            type: String,
            required: true
        },
    },
    doctor: {
        current: Schema.ObjectId, //ObjectId
        all: [
            //{
               // doctor_id: 14, ObjectId
               // started_at: 22123213323, Timestamp
               // ended_at: 5343434223, Timestamp
           // }
        ]
    },
}));

module.exports = User;