const Schema = require('./connector').Schema;
const Model = require('./connector').model;


let Record = Model('Record', new Schema({
    active: Boolean,
    user_id: {
        type: Schema.ObjectId,
        required: true
    },
    doctor_id: {
        type: Schema.ObjectId,
        required: true,
    },
    started_at: Schema.timestamp,
    description: {
        type: String,
        required: true
    },
}));

module.exports = Record;