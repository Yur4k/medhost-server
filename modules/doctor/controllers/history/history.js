let doctorSecret = require('./../../../../DB/jwt').doctorSecret;
let helper = require('./../../../../DB/helpers');
let UserModel = require('./../../../../DB/user');
let HistoryModel = require('./../../../../DB/history');
let jwt = require("jsonwebtoken");


module.exports = (server) => {
    server.post(`/history/requestCode`, (req, res) => {
        let userId = req.body.userId;

        UserModel.findOne({_id: userId}, (error, user) => {
            if (!error && user) {
                let code = Math.floor(Math.random() * 10000);

                user.access_code = code;
                user.save().then(() => {
                    helper.fakeSms(user.contact_info.phones[0], code);
                });
                res.send({success: true, message: "Запрос принят"});
            } else if (!error && !user) {
                res.send({success: false, message: "Ошибка ввода данных"});
            } else {
                res.send({success: false, message: "Внутреняя ошибка. Код ошибки 1-2"});
            }
        });
    });

    server.post(`/history/get`, (req, res) => {
        let doctor = req.header('Doctor-Auth-Token');
        let userId = req.body.userId;

        UserModel.findOne({_id: userId}, (error, user) => {
            if (!error && user) {
                    jwt.verify(doctor, doctorSecret, (error, doctor) => {
                        if(user.allowed === doctor._id) {
                            HistoryModel.findOne({user_id: userId}, ['history_book'], (error, history) => {
                                if (!error && user) {
                                    res.send({success: true, history: history});
                                } else{
                                    res.send({success: false, message: "История пуста"});
                                }
                            })
                        } else {
                            res.send({success: false, message: "Доступ запрещён!"});
                        }
                    });
            }else {
                res.send({success: false, message: "Данных нету."});
            }
        });
    });

    server.post(`/history/open`, (req, res) => {
        let doctor = req.header('Doctor-Auth-Token');
        let userCode = req.body.userCode;
        let userId = req.body.userId;

        UserModel.findOne({_id: userId}, (error, user) => {
            if (!error && user) {
                if (userCode === user.access_code+"" && userCode !== null) {
                    jwt.verify(doctor, doctorSecret, (error, doctor) => {
                        user.allowed = doctor._id;
                    });
                    res.send({success: true, message: "Доступ разрешен"});
                } else {
                    res.send({success: false, message: "Доступ запрещён!"});
                }

                user.access_code = null;
                user.save();
            } else if (!error && !user) {
                res.send({success: false, message: "Ошибка ввода данных"});
            } else {
                res.send({success: false, message: "Внутреняя ошибка. Код ошибки 1-2"});
            }
        });
    });

	server.post(`/history/save`, (req, res) => {
        let userId = req.body.userId;
        let historyPage = req.body.historyPage;
		let doctor = req.header('Doctor-Auth-Token');

        UserModel.findOne({_id: userId}, (error, user) => {
            if (!error && user) {
                jwt.verify(doctor, doctorSecret, (error, doctor) => {
                    if (user.allowed === doctor._id) {
                        user.allowed = undefined;

                        HistoryModel.findOne({user_id: userId}, (error, history) => {
                            history.history_book.push(historyPage);
                            history.save().then(() => {
                                res.send({success: true, message: "Запись успешно добавлена в историю пациента"});

                            });
                        });
                    } else {
                        res.send({success: false, message: "Доступ запрещён!"});
                    }
                });
            } else if (!error && !user) {
                res.send({success: false, message: "Ошибка ввода данных"});
            } else {
                res.send({success: false, message: "Внутреняя ошибка. Код ошибки 1-3"});
            }
        });
	});
};