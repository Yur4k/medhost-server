let DoctorModel = require('./../../../../DB/doctor');
let jwt = require("jsonwebtoken");
let doctorSecret = require('./../../../../DB/jwt').doctorSecret;


module.exports = (server) => {
    server.post(`/auth/login`, (req, res) => {
        let reqData = req.body;
        let token = null;

        DoctorModel.findOne({"contact_info.password": reqData.password, "contact_info.login": reqData.username}, (error, doctor) => {
            if (error === null && doctor === null) {
                res.send({success: false, message: "Введены неправельные идентификационные данные"});
            }else if (error === null && doctor) {
                token = jwt.sign(doctor, doctorSecret, {
                    expiresIn: '10h'
                });
                jwt.verify(token, doctorSecret, (error, decoded) => {
                    res.send({success: true, token, expires: decoded.exp});
                });
            } else {
                res.send({success: false, message: "Ошибка входа. Код ошибки 1-1"});
            }
        });
    });

    server.post(`/auth/validateToken`, (req, res) => {
        let token = req.body.token;

        jwt.verify(token, doctorSecret, (error, decoded) => {
            if (!error && decoded) {
                res.send(true);
            } else {
                res.send(false);
            }
        });
    });
};