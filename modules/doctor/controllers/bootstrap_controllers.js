let history = require('./history/history');
let pacients = require('./pacients/pacients');
let panel = require('./panel/panel');
let auth = require('./auth/auth');


module.exports = (server) => {
	history(server);
    panel(server);
    auth(server);
    pacients(server);
};