let doctor_module = require('./doctor/controllers/bootstrap_controllers');
let terminal_module = () => {};
let user_module = () => {};

module.exports = (server) => {
	doctor_module(server);
	terminal_module(server);
	user_module(server);
};