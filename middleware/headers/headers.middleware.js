//Настраиваем общие заголовки для каждого запроса.
const Headers = (response) => {
    response.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    response.header('Access-Control-Allow-Headers', 'origin, x-requested-with, content-type, Doctor-Auth-Token, Cookie');
    response.header('Access-Control-Allow-Origin', '*');
};


module.exports = Headers;