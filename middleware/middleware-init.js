const Headers = require('./headers/headers.middleware');

//Здесь мы настраиваем промежуточный слой(в частности аутентификацию, заголовки и т.д)
const Middleware = (server) => {
    server.use((request, response, next) => {
        Headers(response);
        //
        //
        next();
    })
};


module.exports = Middleware;